require 'yaml'
require 'fileutils'

required_plugins = %w( vagrant-hostmanager vagrant-vbguest )
required_plugins.each do |plugin|
    exec "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
end

vagrantfile_dir_path = File.dirname(__FILE__)
config = {
    local: vagrantfile_dir_path + '/vagrant/config/vagrant-local.yml',
    example: vagrantfile_dir_path + '/vagrant/config/vagrant-local.example.yml'
}
FileUtils.cp config[:example], config[:local] unless File.exist?(config[:local])
options = YAML.load_file config[:local]

if options['host_name'].nil? || options['host_name'].to_s.length == 0
    puts "You must place host name into configuration:\n/yii2-app-basic/vagrant/config/vagrant-local.yml"
    exit
end

Vagrant.configure(2) do |config|
  config.vm.define options['machine_name']
  config.vm.box = options['machine_box']

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.ignore_private_ip = true
  config.vm.hostname = options['host_name']

  [3000, 3001, 3306, 8086, 1935].each do |port|
      config.vm.network :forwarded_port, guest: port, host: port, host_ip: options['ip']
  end

  config.vm.synced_folder '.', '/home/vagrant/htdocs', :mount_options => %w(dmode=775 fmode=664)

  config.vm.provision :shell, :name => 'provision.sh', :path => 'vagrant/provision.sh', :args => [options['host_name'],
                                                                                                  options['machine_name'],
                                                                                                  options['port']]
  config.vm.provision :shell, :name => 'autostart.sh', :path => 'vagrant/autostart.sh', :run => :always, :privileged => false
end