$(document).ready(function () {
    let emailCheck = new EmailCheck();
    $(emailCheck.settings.selectors.validate).on('click', function (e) {
        e.preventDefault();
        emailCheck.hideEmailStatus();
        emailCheck.checkEmail();
    });
});

// Main logic
class EmailCheck {
    /**
     * @param props
     */
    constructor(props) {
        this.settings = {
            selectors: {validate: '.btn-validate',emailInput: 'form #email', emailStatus: '.email-status'},
            classes: {defaultEmailStatusClass: 'email-status', error: 'email-status--error', success: 'email-status--success'},
            url: {emailCheck: '/api/email/check'},
            translates: {EMAIL_BAD: 'Email address is incorrect', EMAIL_EMPTY: 'Field is empty', EMAIL_TYPE: 'Wrong type, please write correct email address', valid: 'Email is valid'}
        };
    }

    /**
     * @async call to check email on back end side
     */
    checkEmail() {
        $.post(this.settings.url.emailCheck, {email: $(this.settings.selectors.emailInput).val().trim()}, res => {
            if (!res.validate) {
                this.showEmailStatus(res.err, true);
            } else {
                this.showEmailStatus(null, false);
            }
        });
    }

    /**
     * Show validation status in email status label
     * @param code
     * @param isBad
     */
    showEmailStatus(code = 'EMAIL_BAD', isBad) {
        if (isBad) {
            $(this.settings.selectors.emailStatus).text(this.settings.translates[code]).attr('class',this.settings.classes.defaultEmailStatusClass).addClass(this.settings.classes.error).show('fast');
        } else {
            $(this.settings.selectors.emailStatus).text(this.settings.translates.valid).attr('class',this.settings.classes.defaultEmailStatusClass).addClass(this.settings.classes.success).show('fast');
        }
    }

    /**
     * Just hide email state label after each click to validate button
     */
    hideEmailStatus() {
        $(this.settings.selectors.emailStatus).text('').attr('class',this.settings.classes.defaultEmailStatusClass).show('fast');
    }
}
