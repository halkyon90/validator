#!/bin/sh
echo "##################################"
echo "========== Provisions ============"
echo "##################################"

HOST_NAME=$1
MACHINE_NAME=$2
PORT=$3

# Install repos
rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# Install packages
yum -y install nginx mc
yum -y install php71w-common php71w-devel php71w-cli php71w-fpm php71w-mbstring php71w-mcrypt php71w-xml

# PHP configuration
echo "============ php.ini configure changing =============="
sed -i 's/^;date.timezone.*=.*$/date.timezone = UTC/' /etc/php.ini
sed -i 's/^upload_max_filesize.*=.*$/upload_max_filesize = 50M/' /etc/php.ini
sed -i 's/^post_max_size.*=.*$/post_max_size = 50M/' /etc/php.ini

# Stop services
service php-fpm stop
service nginx stop

# Disable Selinux
echo "============ Disabling Selinux =============="
sed -i "s/^SELINUX=.*/SELINUX=disabled/" /etc/selinux/config
echo "Do not forget to reload vagrant by 'vagrant reload' to apply disabling selinux"

# Copy configs
echo "============ Copying main configs =============="
rm -rf /etc/nginx/conf.d/*
cp -r /home/vagrant/htdocs/vagrant/conf/* /etc

# Nginx configuring
echo "============ Rename nginx configs =============="
mv /etc/nginx/sites-enabled/name.vm.conf /etc/nginx/sites-enabled/${HOST_NAME}.conf
echo "============ Main server config =============="
sed -i "s!listen.*!listen ${PORT} default_server;!" /etc/nginx/sites-enabled/${HOST_NAME}.conf
sed -i "s!server_name.*!server_name ${HOST_NAME};!" /etc/nginx/sites-enabled/${HOST_NAME}.conf
sed -i "s!access_log.*!access_log /var/log/nginx/${MACHINE_NAME}_access.log;!" /etc/nginx/sites-enabled/${HOST_NAME}.conf
sed -i "s!error_log.*!access_log /var/log/nginx/${MACHINE_NAME}_access.log;!" /etc/nginx/sites-enabled/${HOST_NAME}.conf

# Start services
service php-fpm start
service nginx start

# Autostart services
chkconfig php-fpm on
chkconfig nginx on
