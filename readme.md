# Email Validator

#### Installation guide:
```bash
git clone {this project}
cd validator
vagrant up # make sure you installed vagrant on your system
# wait for a several minutes, vagrant will install CentOs 7 
# and after installing do not forget make command below to disable selinux!
vagrant reload
# type in browser validator.vm:3000
```