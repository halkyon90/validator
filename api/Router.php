<?php

class Router extends RequestProcessor
{
    const patterns = [
        'POST /api/email/check' => 'check_email'
    ];

    /**
     * Router constructor.
     */
    function __construct ()
    {
        $systemPattern = $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'];
        if (array_key_exists($systemPattern,self::patterns)) {
            $this->call_route(self::patterns[$systemPattern]);
        } else die('404 not found');
    }

    /**
     * @param $funcCall
     */
    function call_route ($funcCall): void
    {
        if(method_exists($this, $funcCall)) $this->$funcCall(); // Если функция присутствует, то выполняем
        else die('Возникла ошибка, ваш запрос не верен!');
    }
}
