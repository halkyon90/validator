<?php

define('DIR', realpath(dirname(__FILE__)));
require_once DIR . '/RequestProcessor.php';
require_once DIR . '/ResponseRender.php';
require_once DIR . '/Router.php';
$routing = new Router;