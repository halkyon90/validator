<?php

class ResponseRender
{
    /**
     * @param array $data
     */
    public static function response(array $data)
    {
        header('Content-Type: application/json');
        echo self::encodeThis($data);
    }

    /**
     * @param string $code
     */
    public static function renderError(string $code)
    {
        header('Content-Type: application/json');
        echo self::encodeThis(['err' => $code]);
    }

    /**
     * @param array $body
     * @return string
     */
    private static function encodeThis(array $body):string
    {
        return json_encode($body,  JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }
}