<?php

class RequestProcessor
{
    const ERROR_CODE_EMAIL_BAD = "EMAIL_BAD";
    const ERROR_CODE_EMAIL_EMPTY = "EMAIL_EMPTY";
    const ERROR_CODE_EMAIL_TYPE = "EMAIL_TYPE";

    const REGULAR_CODE_EMAIL_VALID = true;
    // FIXME looks like EMAIL_BAD code replaced it, correct me if I'm wrong...
    const REGULAR_CODE_EMAIL_UNVALID = false;

    /**
     * Check email through several conditions
     */
    function check_email()
    {
        try {
            /* @var $emailVar string */
            $emailVar = $_POST['email'] ?? '' ;

            if (!is_null($emailVar)) $emailVar = trim($emailVar);
            if (is_null($emailVar) || empty($emailVar) || $emailVar == '') {
                ResponseRender::renderError(self::ERROR_CODE_EMAIL_EMPTY);
            } else {
                if (gettype($emailVar) !== 'string') {
                    ResponseRender::renderError(self::ERROR_CODE_EMAIL_TYPE);
                } else if (self::validateEmail($emailVar)) {
                    ResponseRender::response(['validate' => self::REGULAR_CODE_EMAIL_VALID]);
                } else {
                    ResponseRender::renderError(self::ERROR_CODE_EMAIL_BAD);
                }
            }
        } catch (Exception $e) {
            ResponseRender::renderError(self::ERROR_CODE_EMAIL_BAD);
        }
    }

    /**
     * Validate email using quite a simple regular expression
     * @param string $email
     * @return false|int
     */
    private static function validateEmail(string $email)
    {
        return preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email);
    }
}
